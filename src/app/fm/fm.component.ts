import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { InteractionService } from '../interaction.service';

@Component({
  selector: 'app-fm',
  templateUrl: './fm.component.html',
  styleUrls: ['./fm.component.css']
})
export class FmComponent implements OnInit {

  public userForm: FormGroup
  toDate: string = "";
  fromDate: string = "";
  dateType: string = "";
  data: string[] = [];
  mData: string[] = [];
  //mToDate: string = "";
  //mFromDate: string = "";
  tdate: any;
  fdate: any;
  public radioButtonCheck :boolean;
  public spanStyle:string="";

  constructor(private fb: FormBuilder, private _interactionService: InteractionService) {
    this.userForm = this.fb.group({
      fromDate: "",
      toDate: "",
      dataType: ""
    });
    this.radioButtonCheck=false
  }


  /* date = new FormGroup({
     dateType: new FormControl('', Validators.required)
   });
     */
  setValue() {
    this.toDate = this.userForm.get('toDate')?.value;
    this.fromDate = this.userForm.get('fromDate')?.value;
    /*
    if(parseInt(this.toDate.slice(0,4))<parseInt((this.fromDate.slice(0,4))))
    {
      console.warn("invalid date")
      this.flag=0
    }
    else if(parseInt(this.toDate.slice(0,4))==parseInt((this.fromDate.slice(0,4)))){
      if(parseInt(this.toDate.slice(5,7))<parseInt((this.fromDate.slice(5,7))))
      {
        console.warn("invalid date2")
        this.flag=0
      }
      else if(parseInt(this.toDate.slice(5,7))==parseInt((this.fromDate.slice(5,7))))
      {
        if(parseInt(this.toDate.slice(8,10))<parseInt((this.fromDate.slice(8,10))))
        {
          console.log("invalid date3")
          this.flag=0
        }
      }
    }
    if(this.data.length==0 && this.flag){

      //ddmmyy mmddyy
      this.data.push(this.toDate)
      this.data.push(this.fromDate)
      //console.warn(this.date.value)
      const ctoDate2 = new Date (this.toDate)
      console.log(ctoDate2)
      ////change 
      //this.mFromDate=this.mFromDate.concat(this.fromDate.slice(8,10).toString(),s["-",this.toDate.slice(5,7).toString(),"-",this.toDate.slice(0,4).toString()])
      this.mToDate.concat(this.toDate.slice(8,10),this.toDate.slice(5,7),this.toDate.slice(0,4))
      console.warn(this.mFromDate)
      this.mData.push(this.mFromDate)
    //this.mData.push(this.mToDate)

      if(this.dateType==="dd-mm-yy")
      {
        //this.toDate.slice(8,10)+this.toDate.slice(5,7)+this.toDate.slice(0,4)
        this._interactionService.sendMessage(this.mData)
      }
      else{
        this._interactionService.sendMessage(this.data)
      }

    }*/
    //console.log(this.dateType)
    this.tdate = new Date(this.toDate)
    this.fdate = new Date(this.fromDate)

    /*this.data.push(this.toDate)
    this.data.push(this.fromDate)*/

    if (this.dateType === "dd-mm-yy") {
      this.changeDateOne()
    }
    else if (this.dateType === "mm-dd-yy") {
      this.changeDateTwo()
    }
    this.spanStyle="distance2"
  }
  radioChangeHandler(event: any) {
    this.radioButtonCheck=true
    this.dateType = event.target.value
    if (this.dateType === "dd-mm-yy") {
      this.changeDateOne()
    }
    else if (this.dateType === "mm-dd-yy") {
      this.changeDateTwo()
    }
  }
  changeDateOne(){
    this.fromDate = this.fdate.getDate() + "-" + (this.fdate.getMonth() + 1) + "-" + this.fdate.getFullYear()
    this.toDate = this.tdate.getDate() + "-" + (this.tdate.getMonth() + 1) + "-" + this.tdate.getFullYear()
  }

  changeDateTwo(){
    this.fromDate = (this.fdate.getMonth() + 1) + "-" + this.fdate.getDate() + "-" + this.fdate.getFullYear()
    this.toDate = (this.tdate.getMonth() + 1) + "-" + this.tdate.getDate() + "-" + this.tdate.getFullYear()
  }

  ngOnInit(): void {
    this.spanStyle="distance"
  }

}
