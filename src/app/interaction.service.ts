import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
//import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class InteractionService {

  private _teacherMessageSource =new Subject<Array<string>>();
  teacherMessage$ = this._teacherMessageSource.asObservable();
  constructor() { }

  sendMessage(message:Array<string>){
    this._teacherMessageSource.next(message)
    console.warn(message)
     //this.http.post(this.url,message)

  }
}
